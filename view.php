<?php

    require __DIR__ . '/users/users.php';

    include 'layout/header.php';


        $userId = $_GET['id'];

        if (isset($_GET['id'])) {


            $user = getUserById($userId);

            if (!$user) {

               include 'layout/not_found.php';
               exit; 
            }

        } else {

            include 'layout/not_found.php';
            exit;
        } 


?>

<div class="container">
<div class="card">
        <div class="card-header">
            <h3 class="display-6">View User: <?= $user['name']; ?></h3>
        </div>
        <div class="card-body">
            <a href="update.php?id=<?= $user['id']; ?>" class="btn btn-secondary">Update</a>
            <form style="display:inline-block" action="delete.php" method='post'>
                    <input type="hidden" name='id' value="<?= $user['id']; ?>">
                    <button class="btn btn-danger">Delete</button>
            </form>
        </div>
        <table class="table">
            <tbody>
                <tr>
                    <th>Name:</th>
                    <td><?= $user['name']; ?></td>
                </tr>

                <tr>
                    <th>Username:</th>
                    <td><?= $user['username']; ?></td>
                </tr>

                <tr>
                    <th>Email:</th>
                    <td><?= $user['email']; ?></td>
                </tr>

                <tr>
                    <th>Phone:</th>
                    <td><?= $user['phone']; ?></td>
                </tr>

                <tr>
                    <th>WebSite:</th>
                    <td><a target="_blank" href="<?= $user['website']; ?>"><?= $user['website'] ?></a></td>
                </tr>
            </tbody>    

        </table>
</div>

</div>



<?php include 'layout/footer.php'; ?>