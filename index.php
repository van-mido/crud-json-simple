<?php

    require __DIR__ . '/users/users.php';

    $users = getUsers();

    // print_r($users);

    include 'layout/header.php';

?>

<div class="container">
<p><a href="./create.php" class="btn btn-success mt-2">Create User</a></p>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">Image</th>
        <th scope="col">Name</th>
        <th scope="col">Username</th>
        <th scope="col">Email</th>
        <th scope="col">Phone</th>
        <th scope="col">WebSite</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
  
      <?php foreach ($users as $user): ?>
            <tr>

                  
                        <td>
                            <?php if (isset($user['extension'])): ?>
                              
                                <img style="width: 60px"src="<?= "users/images/{$user['id']}.{$user['extension']}" ?>" alt="">

                            <?php endif; ?>
                        
                        </td>
                        <td><?= $user['name']; ?></td>
                        <td><?= $user['username']; ?></td>
                        <td><?= $user['email']; ?></td>
                        <td><?= $user['phone']; ?></td>
                        <td><a target="_blank" href="<?= $user['website']; ?>"><?= $user['website']; ?></a</td>
                        <td>
                            <a href="./view.php?id=<?= $user['id']; ?>" class="btn btn-sm btn-outline-info ml-1">View</a>
                            <a href="./update.php?id=<?= $user['id']; ?>" class="btn btn-sm btn-outline-secondary ml-1">Update</a>
                            <form action="delete.php" method='post'>
                                <input type="hidden" name='id' value="<?= $user['id']; ?>">
                                <button class="btn btn-sm btn-outline-danger">Delete</button>
                            </form>
                        </td>

            </tr>
      <?php endforeach; ?> 


    </tbody>
  </table>
</div>

  

<?php include 'layout/footer.php'; ?>