<?php

    require __DIR__ . '/users/users.php';

    include 'layout/header.php';


        $userId = $_GET['id'];

        if (isset($_GET['id'])) {


            $user = getUserById($userId);

            // echo '<pre>';
            //     var_dump($user);
            // echo '</pre>';

            // exit;

            if (!$user) {

               include 'layout/not_found.php';
               exit; 
            }

        } else {

            include 'layout/not_found.php';
            exit;
        }      


        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            // echo '<pre>';
            //       var_dump($_SERVER);
            // echo '</pre>';

            // var_dump($_POST);
            // exit;


            $user = updateUser($_POST, $userId);

            //Probamos los files
            // echo '<pre>';
            //     var_dump($_FILES);
            // echo '</pre>';
             // exit;

            if (isset($_FILES['picture'])) {

                uploadImage($_FILES['picture'], $user);

            }

            header('Location: ./index.php');

        }

?>

    <?php include '_form.php'; ?>


<?php include 'layout/footer.php'; ?>

