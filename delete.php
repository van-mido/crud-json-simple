<?php

require __DIR__ . '/users/users.php';

include 'layout/header.php';

// echo '<pre>';
//         var_dump($_POST);
//         echo '</pre>';
//         exit;

    if (!isset($_POST['id'])) {

            include 'layout/not_found.php';
            exit;

        }

        $userId = $_POST['id'];

        deleteUser($userId);

    header('Location: ./index.php');