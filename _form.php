<div class="container">
    <div class="card">
        <div class="card-header">
            <h3 class="display-6">
                <?php if ($user['id']): ?>
                    Update User: <b><?= $user['name']; ?></b>    
                <?php else: ?>
                    Create new user
                <?php endif; ?>          
            </h3>
        </div>
        <div class="card-body">
                <form method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control<?= isset($errors['name']) ? ' is-invalid' : ''; ?>" id="name" name='name' value="<?= $user['name']; ?>">
                        <div class="invalid-feedback">
                            <?= isset($errors['name']) ? $errors['name'] : ''; ?>
                        </div>    
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control<?= isset($errors['username']) ? ' is-invalid' : ''; ?>" id="username" name='username' value="<?= $user['username']; ?>">
                        <div class="invalid-feedback">
                            <?= isset($errors['username']) ? $errors['username'] : ''; ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control<?= isset($errors['email']) ? ' is-invalid' : ''; ?>" id="email" name='email' value="<?= $user['email']; ?>">
                        <div class="invalid-feedback">
                            <?= isset($errors['email']) ? $errors['email'] : ''; ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label for="name">Phone</label>
                        <input type="text" class="form-control<?= isset($errors['phone']) ? ' is-invalid' : ''; ?>" id="phone" name='phone' value="<?= $user['phone']; ?>">
                        <div class="invalid-feedback">
                            <?= isset($errors['phone']) ? $errors['phone'] : ''; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Website">WebSite</label>
                        <input type="text" class="form-control" id="phone" name='website' value="<?= $user['website']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" class="form-control-file" id="image" name="picture">  
                    </div>  
                    <button type="submit" class="btn btn-outline-primary">Enviar</button>
                </form>            
        </div>       
    </div>       
</div>

