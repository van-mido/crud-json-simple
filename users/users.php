<?php


    function getUsers()
    {

        // Convertimos un json -> array
        return json_decode(file_get_contents(__DIR__ . '/users.json'), true);

        // echo '<pre>';

        // var_dump($users);

        // foreach ($users as $user) {

        //         print_r($user->fetch());
        // }

        // while ($row = $users->fetch()) {

        //         print_r($row);
        // }

        // echo '</pre>';

    }

    function getUserById($id)
    {

           $users = getUsers();

           foreach ($users as $user) {

                if ($user['id'] == $id) {

                    return $user;
                }
           }

           return null;

    }

    function createUser($data)
    {

           $users = getUsers();

           $data['id'] = rand(100000, 200000);

           $users[] = $data;

           putJson($users);

           return $data;

           

    }


    function updateUser($data, $id)
    {

            $updateUser = [];

            $users = getUsers();
            
            foreach ($users as $key => $user) {

                if ($user['id'] == $id) {

                    // echo '<pre>';
                    //     var_dump($users, $data, array_merge($user, $data));
                    // echo '</pre>';
                    // exit;

                   $users[$key] = $updateUser = array_merge($user, $data);    
                     
                }


            }

            putJson($users);

            return $updateUser;

    }

    function deleteUser($id)  
    {

            $users = getUsers();

            foreach ($users as $i => $user) {

                if ($user['id'] == $id) {

                    // Borrara el usuario 
                    // unset($users[$i]);
                    array_splice($users, $i, 1);

                }
            }

            // echo '<pre>';
            // var_dump($users);
            // echo '</pre>';

            putJson($users);


    }

    function uploadImage($file, $user)
    {

        if (isset($_FILES['picture']) && $_FILES['picture']['name']) {


                if (!is_dir(__DIR__ . '/images')) {

                    mkdir(__DIR__ . '/images');
                }


                // Capturamos la extension del archivo
                $fileExtension = $file['name'];
                // Ubicamos el 'punto' en la extension del archivo
                $dotPosition = strpos($fileExtension, '.');
                // Ahora capturamos la extension del archivo para evaluarla
                $extension = substr($fileExtension, $dotPosition + 1);

                // echo '<pre>';
                //     var_dump($extension);
                // echo '</pre>';    


                move_uploaded_file($file['tmp_name'], __DIR__ . "/images/{$user['id']}.$extension");

                // Escribira la fila de la extension del archivo en el documento JSON
                $user['extension'] = $extension;
                updateUser($user, $user['id']);

            }

        
    }

    function putJson($users)
    {

        file_put_contents(__DIR__ . '/users.json', json_encode($users, JSON_PRETTY_PRINT));

    }

    function validateUser($user)
    {

        $formTrigger = true;

        if (!$user['name']) {

            $errors['name'] = 'The Name is mandatory'; 
            $formTrigger = false;

        } 
        
        if (!$user['username'] || strlen($user['username']) < 6 || strlen($user['username']) > 16) {

            $errors['username'] = 'Username is required and it must be more than 6 and less then 16 characters';    
            $formTrigger = false;

        }

        if (!$user['email'] || !filter_var($user['email'], FILTER_VALIDATE_EMAIL)) {

            $errors['email'] = 'This must be a valid email address';
            $formTrigger = false;    
        }

        if (!filter_var($user['phone'], FILTER_VALIDATE_INT) || !$user['phone']) {

            $errors['phone'] = 'This must be a valid phone-number';
            $formTrigger = false;  
        }

        return  $formTrigger;

    }