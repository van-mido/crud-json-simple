<?php

    include 'layout/header.php';

    require __DIR__ . '/users/users.php';
    
    // Reemplazamos los valores que vienen predefinidos en el formulario
    $user = [

        'id' => '',
        'name' => '',
        'username' => '',
        'email' => '',
        'phone' => '',
        'website' => ''

    ];

    // Validador si el fomulario tiene los requisitos de ingreso
    $formTrigger = true;

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        $user = array_merge($user, $_POST);

        // Funcion que nos ayuda a validar los campos del formulario
        // $formTrigger = validateUser($user);

        // Validamos formulario
        if (!$user['name']) {

            $errors['name'] = 'The Name is mandatory'; 
            $formTrigger = false;

        } 
        
        if (!$user['username'] || strlen($user['username']) < 6 || strlen($user['username']) > 16) {

            $errors['username'] = 'Username is required and it must be more than 6 and less then 16 characters';    
            $formTrigger = false;

        }

        if (!$user['email'] || !filter_var($user['email'], FILTER_VALIDATE_EMAIL)) {

            $errors['email'] = 'This must be a valid email address';
            $formTrigger = false;    
        }

        if (!filter_var($user['phone'], FILTER_VALIDATE_INT) || !$user['phone']) {

            $errors['phone'] = 'This must be a valid phone-number';
            $formTrigger = false;  
        }


            if ($formTrigger) {

                $user = createUser($_POST);

                if (isset($_FILES['picture'])) {

                    uploadImage($_FILES['picture'], $user); 

                }

                header('Location: ./index.php');

            }

            
        

        
                
    }


?>

    <?php include '_form.php'; ?>

<?php include 'layout/footer.php'; ?>